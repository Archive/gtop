#!/bin/sh
# Run this to generate all the initial makefiles, etc.

REQUIRED_AUTOMAKE_VERSION=1.7.2
# We require Automake 1.7.2, which requires Autoconf 2.54.
REQUIRED_AUTOCONF_VERSION=2.54

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="GNOME System Monitor"

(test -f $srcdir/configure.in \
  && test -f $srcdir/details.c ) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    exit 1
}

. gnome-autogen.sh
